package com.springcrud.repository;

import java.util.List;

import com.springcrud.controller.Library;

public interface LibraryRepositoryCustom {
	
	List<Library> findAllByAuthor(String authorName);

}
