package Basic.Selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class InvokeBrowser {
	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "C:\\Users\\home\\Desktop\\SeleniumJars\\chromedriver.exe");

		WebDriver driver = new ChromeDriver(); // run time
		driver.get("http://demo.guru99.com/test/newtours/register.php");

		driver.manage().window().maximize();
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		driver.findElement(By.name("firstName")).sendKeys("sharath");
		driver.findElement(By.name("lastName")).sendKeys("durgam");
		driver.findElement(By.name("phone")).sendKeys("987654321");
		// driver.findElement(By.name("")).sendKeys("abc@yahoomail.com");
		driver.findElement(By.id("userName")).sendKeys("sharathDurgam05");
		driver.findElement(By.name("address1")).sendKeys("Hermmanplatz");
		driver.findElement(By.name("city")).sendKeys("Berlin");
		driver.findElement(By.name("state")).sendKeys("Berlin");
		driver.findElement(By.name("postalCode")).sendKeys("18740");
		WebElement staticDropdown = driver.findElement(By.name("country"));
		Select dropDown = new Select(staticDropdown);
		dropDown.selectByValue("INDIA");
		driver.findElement(By.xpath("//tbody/tr[11]/td[2]/select[1]"));
		driver.findElement(By.id("email")).sendKeys("sharathDurgam05@gmail.com");
		driver.findElement(By.name("password")).sendKeys("12345678");
		driver.findElement(By.name("confirmPassword")).sendKeys("12345678");
		driver.findElement(By.name("submit")).click();
		driver.findElement(By.xpath("//a[contains(text(),'sign-in')]")).click();
		System.out.println(driver.findElement(By.xpath("//tbody/tr[3]/td[1]/p[2]/font[1]")).getText());
		// System.out.println("Hello World!");
	}
}
