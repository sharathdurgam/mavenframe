package Basic.Selenium;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AdvanceDropdown {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\home\\Desktop\\SeleniumJars\\chromedriver.exe");

		WebDriver driver = new ChromeDriver(); // run time
		driver.get("http://www.amazon.com");

		driver.manage().window().maximize();
		System.out.println(driver.getTitle());
		
		WebElement seldropdown = driver.findElement(By.id("searchDropdownBox"));
		
		Select sel = new Select(seldropdown);
		
		List<WebElement> li = sel.getOptions();
		
		System.out.println(li.size());
		
//		for(int i = 0 ; i<li.size() ; i++) // 0 0<=27
//		{
//			//li.get(i).click();
//			//System.out.println(li.get(i).getText()); //
//			
//			//10==10
//			
//			if(li.get(i).getText().equalsIgnoreCase("Electronics"))
//			{
//				li.get(i).click();
//			}
//		}
		for(int i= li.size()-1; i>=0; i--) {
			System.out.println(li.get(i).getText());
		}

	}

}
