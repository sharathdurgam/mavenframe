package Basic.Selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class AutomationE2E {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\home\\Desktop\\SeleniumJars\\chromedriver.exe");
		WebDriver driver = new ChromeDriver(); // run time
		driver.manage().window().maximize();
		driver.get("http://automationpractice.com/index.php");
		Actions act = new Actions(driver);
		act.moveToElement(driver.findElement(By.xpath("//a[text()='Women']"))).perform();
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//a[text()='Casual Dresses'])[1]")).click();
		Thread.sleep(2000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,700)");
		Thread.sleep(2000);
		act.moveToElement(driver.findElement(By.xpath("//img[@title='Printed Dress']"))).perform();
		driver.findElement(By.xpath("//span[text()='Quick view']")).click();
		Thread.sleep(2000);
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@class='fancybox-iframe']")));
		driver.findElement(By.id("quantity_wanted")).click();
		for (int i = 0; i < 5; i++) {
			driver.findElement(By.xpath("//i[@class='icon-plus']")).click();
			Thread.sleep(2000);
		}
		WebElement staticDropdown = driver.findElement(By.xpath("//select[@name='group_1']"));
		Select dropDown = new Select(staticDropdown);
		dropDown.selectByValue("2");
		driver.findElement(By.xpath("//span[text()='Add to cart']")).click();
		driver.switchTo().defaultContent();
		Thread.sleep(2000);
//		act.moveToElement(driver.findElement(By.xpath("//div[@class='clearfix']"))).perform();
//		WebElement loginButtonId = driver.findElement(By.xpath("//a[@title='Proceed to checkout']"));
//		loginButtonId.click();
		// act.moveToElement(driver.findElement(By.xpath("//div[@class='clearfix']"))).perform();
		//driver.findElement(By.xpath("//div[@class='clearfix']")).click();
		// act.moveToElement(driver.findElement(By.xpath("//div[@class='layer_cart_cart col-xs-12 col-md-6']"))).perform();
		 driver.findElement(By.xpath("//a[@title='Proceed to checkout']")).click();
		//driver.findElement(By.xpath("//a[@title='Proceed to checkout']")).click();
	}

}
