package Basic.Selenium;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ArrayListDemo {

	public static void main(String[] args) {

		//ArrayList al = new ArrayList();
		//ArrayList<String> al = new  ArrayList<String>();
		List<Object> l = new ArrayList<>();
		
		l.add("Hello");
		l.add("welcome");
		l.add("to");
		l.add("java");
		l.add("Collections");
		l.add("Topic ");
		l.add("practic");
		System.out.println(l);
		
		for(Object elements:l) {
			System.out.println("values with for each loop:"+elements);
		}
		
		
		l.add(1, "Everyone"); // add element by index
		System.out.println(l);
		
		l.add("all the best"); // add by object
		System.out.println(l);

		l.remove(2); // removing element by index
		System.out.println(l);
		
		ArrayList<Object> al = new ArrayList<Object>();
		al.addAll(l);  // addAll is to adding all elements to new Arrylist
		System.out.println("Arrylist objects copied by List:"+al);
		
		al.removeAll(l);
		System.out.println("Arrylist objects removed:"+al);
		
		l.get(2);// getting single element
		System.out.println(l.get(2));
		
		
		l.set(1, "All");
		System.out.println(l);
		
		l.isEmpty(); // checks the its empty or not
		System.out.println(l.isEmpty());
		
		l.contains("Collections"); // check whether element contains or not
		System.out.println(l.contains("Collections"));
		
		//Collections.sort(l); // sorting assending order
		System.out.println(l);
		
		Collections.sort(l,Collections.reverseOrder()); // sorting desending order
		System.out.println(l);
		
		
		Collections.shuffle(l);
		System.out.println(l);
		
		
		
	}

}
