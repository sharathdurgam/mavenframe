package Basic.Selenium;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Amazon {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\home\\Desktop\\SeleniumJars\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		driver.get("https://www.amazon.com/");
		driver.manage().window().maximize();

		WebElement accountlist = driver.findElement(By.xpath("//span[text()='Account & Lists']"));

		Actions act = new Actions(driver);

		// act.moveToElement(driver.findElement(By.id("nav-link-accountList"))).build().perform();
		act.moveToElement(accountlist).perform();
		// WebElement selectList = driver.findElement(By.id("nav-al-container"));

		List<WebElement> li = driver.findElements(By.xpath("//span[@class='nav-text']"));

		for (WebElement allElements : li) {
			System.out.println(allElements.getText());
		}
//		System.out.println(li.size());
//
//		driver.findElement(By.xpath("//span[text()='Account']")).click();
//		// act.moveToElement(driver.findElement(By.xpath("//input[@aria-label='Search']"))).click().keyDown(Keys.SHIFT).sendKeys("sharath").doubleClick().build().perform();

	}

}
