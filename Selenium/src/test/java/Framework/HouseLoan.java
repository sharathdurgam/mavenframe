package Framework;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class HouseLoan {
	WebDriver driver;

	@BeforeTest
	public void amzonlogin() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\home\\Desktop\\SeleniumJars\\chromedriver.exe");

		driver = new ChromeDriver(); // run time
		driver.manage().window().maximize();
		driver.get("http://www.amazon.com");
	}

	@Test
	public void searchDropDown() {
		WebElement seldropdown = driver.findElement(By.id("searchDropdownBox"));

		Select sel = new Select(seldropdown);

		List<WebElement> li = sel.getOptions();

		System.out.println(li.size());
	}

	@Test
	public void mouseHovering() {

		WebElement accountlist = driver.findElement(By.xpath("//span[text()='Account & Lists']"));

		Actions act = new Actions(driver);

		act.moveToElement(accountlist).perform();

		List<WebElement> li = driver.findElements(By.xpath("//span[@class='nav-text']"));

		for (WebElement allElements : li) {
			System.out.println(allElements.getText());
		}
	}

	@AfterSuite
	public void CleanUp() {
		driver.close();
	}

}
