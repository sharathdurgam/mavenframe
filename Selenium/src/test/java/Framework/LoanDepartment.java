package Framework;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class LoanDepartment {
	@BeforeTest
	public void loan() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\home\\Desktop\\SeleniumJars\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		driver.get("https://www.amazon.com/");
		driver.manage().window().maximize();

		WebElement accountlist = driver.findElement(By.xpath("//span[text()='Account & Lists']"));

		Actions act = new Actions(driver);

		// act.moveToElement(driver.findElement(By.id("nav-link-accountList"))).build().perform();
		act.moveToElement(accountlist).perform();
		// WebElement selectList = driver.findElement(By.id("nav-al-container"));

		List<WebElement> li = driver.findElements(By.xpath("//span[@class='nav-text']"));

		for (WebElement allElements : li) {
			System.out.println(allElements.getText());
		}

		System.out.println("loan working");
	}

	@Test
	public void registrationProcess() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\home\\Desktop\\SeleniumJars\\chromedriver.exe");

		WebDriver driver = new ChromeDriver(); // run time
		driver.get("http://demo.guru99.com/test/newtours/register.php");

		driver.manage().window().maximize();
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		driver.findElement(By.name("firstName")).sendKeys("sharath");
		driver.findElement(By.name("lastName")).sendKeys("durgam");
		driver.findElement(By.name("phone")).sendKeys("987654321");
		// driver.findElement(By.name("")).sendKeys("abc@yahoomail.com");
		driver.findElement(By.id("userName")).sendKeys("sharathDurgam05");
		driver.findElement(By.name("address1")).sendKeys("Hermmanplatz");
		driver.findElement(By.name("city")).sendKeys("Berlin");
		driver.findElement(By.name("state")).sendKeys("Berlin");
		driver.findElement(By.name("postalCode")).sendKeys("18740");
		WebElement staticDropdown = driver.findElement(By.name("country"));
		Select dropDown = new Select(staticDropdown);

		List<WebElement> li = dropDown.getOptions();

		for (int i = 0; i < li.size(); i++) {
			System.out.println(li.get(i).getText());

			if (li.get(i).getText().equalsIgnoreCase("INDIA")) {

				li.get(i).click();

				break;

			}
		}
	}

	@Test
	public void loanProcessFinished() {
		System.out.println("loan process Finished");
	}

}