package Basic.Selenium;

import java.util.List;

import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

public class Register {

	WebDriver driver;

	@BeforeClass
	public void driverSetup() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\home\\Desktop\\SeleniumJars\\chromedriver.exe");

		driver = new ChromeDriver(); // run time
	}
    @Test
	public void invokeBrowser() {
		driver.manage().window().maximize();
		driver.get("http://demo.guru99.com/test/newtours/register.php");
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());

	}

	@Test
	public void typingDetails() {
		driver.findElement(By.name("firstName")).sendKeys("sharath");
		driver.findElement(By.name("lastName")).sendKeys("durgam");
		driver.findElement(By.name("phone")).sendKeys("987654321");
		driver.findElement(By.id("userName")).sendKeys("sharathDurgam05");
		driver.findElement(By.name("address1")).sendKeys("Hermmanplatz");
		driver.findElement(By.name("city")).sendKeys("Berlin");
		driver.findElement(By.name("state")).sendKeys("Berlin");
		driver.findElement(By.name("postalCode")).sendKeys("18740");
		WebElement staticDropdown = driver.findElement(By.name("country"));
		Select dropDown = new Select(staticDropdown);
		List<WebElement> li = dropDown.getOptions();
		for (int i = 0; i < li.size(); i++) {
			System.out.println(li.get(i).getText());

			if (li.get(i).getText().equalsIgnoreCase("INDIA")) {

				li.get(i).click();

				break;

			}
		}
		driver.findElement(By.xpath("//tbody/tr[11]/td[2]/select[1]"));
		driver.findElement(By.id("email")).sendKeys("sharathDurgam05@gmail.com");
		driver.findElement(By.name("password")).sendKeys("12345678");
		driver.findElement(By.name("confirmPassword")).sendKeys("12345678");
		driver.findElement(By.name("submit")).click();
		driver.findElement(By.xpath("//a[contains(text(),'sign-in')]")).click();
	}

	@AfterSuite
	public void CleanUp() {
		driver.close();
	}

}
