package Basic.Selenium;

import java.util.Iterator;
import java.util.Set;

import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class windowHandling {
	WebDriver driver;
	
	@BeforeTest
	public void driverSetup() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\home\\Desktop\\SeleniumJars\\chromedriver.exe");

		driver = new ChromeDriver(); // run time
	}

	@Test
	public void windowsChanging() {

		driver.manage().window().maximize();
		driver.get("https://www.rahulshettyacademy.com/loginpagePractise/");
		driver.findElement(By.xpath("//a[@class='blinkingText']")).click();
		Set<String> window = driver.getWindowHandles();
		Iterator<String> it = window.iterator();
		String ParentId = it.next();
		String ChildId = it.next();
		driver.switchTo().window(ChildId);
		System.out.println(driver.findElement(By.xpath("//p[contains(text(),'email ')]")).getText());
		driver.switchTo().window(ParentId);
	}

	@BeforeMethod
	public void beforeWindow() {
		System.out.println("windows before changing....its under the process");
	}

	@AfterMethod
	public void afterWindow() {
		System.out.println("windows after changing .......done");
	}

}
