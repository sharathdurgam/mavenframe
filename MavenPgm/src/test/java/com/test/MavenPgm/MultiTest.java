package com.test.MavenPgm;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MultiTest {
	
	WebDriver driver;
	
	@BeforeClass
	public void Initalization()
	{
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\home\\Desktop\\SeleniumJars\\chromedriver.exe");

	driver = new ChromeDriver(); // run time
	driver.get("http://www.amazon.com");

	driver.manage().window().maximize();
	System.out.println(driver.getTitle());

		
	}
	
	@Test(priority=1)
	public void SelectDropDown() {
		
				
		WebElement seldropdown = driver.findElement(By.id("searchDropdownBox"));
		
		Select sel = new Select(seldropdown);
		
		List<WebElement> li = sel.getOptions();
		
		System.out.println(li.size());
		
		
	}
	
	@Test(priority=2)
	public void MouseHovering()
	{
		WebElement accountlist = driver.findElement(By.xpath("//span[text()='Account & Lists']"));

		Actions act = new Actions(driver);

		// act.moveToElement(driver.findElement(By.id("nav-link-accountList"))).build().perform();
		act.moveToElement(accountlist).perform();
		// WebElement selectList = driver.findElement(By.id("nav-al-container"));

		List<WebElement> li = driver.findElements(By.xpath("//span[@class='nav-text']"));

		for (WebElement allElements : li) {
			System.out.println(allElements.getText());
		}
		
		
	}
	
	@AfterClass
	public void CleanUp()
	{
		driver.close();
	}

}
